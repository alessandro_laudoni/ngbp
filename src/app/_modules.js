angular.module('ngbp', ['ui.router', 'templates-common', 'ngBoilerplate.home', 'ngBoilerplate.about', 'templates-app'])

.constant('app', 'ngbp')

.constant('VERSION', '0.0.1')

.constant('ENV', {name:'production',apiEndpoint:''})

;